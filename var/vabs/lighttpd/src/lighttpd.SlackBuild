#!/bin/sh

# Slackware build script for lighttpd

# Copyright (c) 2007 Daniel de Kok <moc.mikciat@leinad>
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
# EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

PRGNAM=lighttpd
VERSION=1.4.32
BUILD=${BUILD:-1}
TAG=${TAG:-"$(ls /var/log/packages/|grep vlconfig2|cut -d "-" -f4|cut -c 2-5)"}
LINK=${LINK:-"http://download.lighttpd.net/${PRGNAM}/releases-1.4.x/${PRGNAM}-${VERSION}.tar.bz2"}

if [ -z "$ARCH" ]; then
  case "$( uname -m )" in
    i?86) ARCH=i586 ;;
    arm*) ARCH=arm ;;
       *) ARCH=$( uname -m ) ;;
  esac
fi

CWD=$(pwd)
TMP=${TMP:-/tmp/SBo}
PKG=$TMP/package-$PRGNAM
OUTPUT=${OUTPUT:-${CWD}/..}

if [ "$NORUN" != 1 ]; then

## lighttpd user & group *MUST* exist before package creation
# See http://slackbuilds.org/uid_gid.txt for current recomendations.
LIGHTTPD_USER=${LIGHTTPD_USER:-lighttpd}
LIGHTTPD_GROUP=${LIGHTTPD_GROUP:-lighttpd}

create_user() {
	useradd -u 208 -g $LIGHTTPD_GROUP -d /var/www $LIGHTTPD_USER || exit 1
}
create_group() {
	groupadd -g 208 $LIGHTTPD_GROUP || exit 1 
}


if ! grep -q ^$LIGHTTPD_GROUP: /etc/group 2>/dev/null ; then
	echo "group $LIGHTTPD_GROUP does not exist.  In 30 seconds, we will create it."
	echo "Press CTRL+C to stop"
	sleep 30
	create_group
fi

if ! grep -q ^$LIGHTTPD_USER: /etc/passwd 2>/dev/null ; then
	echo "user $LIGHTTPD_USER does not exist.  In 30 seconds, we will create it."
	echo "Press CTRL+C to stop"
	sleep 30
	create_user
fi

if [ "$ARCH" = "i?86" ]; then
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC"
  LIBDIRSUFFIX="64"
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
fi

# Download the source
for link in $(echo $LINK); do
	(
	cd $CWD
	wget -c --no-check-certificate $link
	)
done

set -e # Exit on most errors

rm -rf $PKG
mkdir -p $TMP $PKG $OUTPUT
cd $TMP
rm -rf $PRGNAM-$VERSION
tar xvf $CWD/$PRGNAM-$VERSION.tar.bz2 || exit 1
cd $PRGNAM-$VERSION
chown -R root:root .
chmod -R u+w,go+r-w,a-s .

CFLAGS="$SLKCFLAGS" \
CXXFLAGS="$SLKCFLAGS" \
./configure \
  --prefix=/usr \
  --libdir=/usr/lib${LIBDIRSUFFIX}/$PRGNAM \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --mandir=/usr/man \
  --with-pcre \
  --with-attr \
  --with-openssl \
  --build=$ARCH-slackware-linux || exit 1

make || exit 1
make install-strip DESTDIR=$PKG || exit 1

mkdir -p $PKG/var/{cache,log}/$PRGNAM
chmod 0700 $PKG/var/cache/lighttpd
mkdir -p $PKG/var/www/htdocs-lighttpd
touch $PKG/var/log/lighttpd/{access,error}.log.new

# Create the default pid file directory (configurable in lighttpd.conf)
mkdir -p $PKG/var/run/lighttpd
chown $LIGHTTPD_USER:$LIGHTTPD_GROUP $PKG/var/run/lighttpd

install -D -m 0755 $CWD/rc.$PRGNAM $PKG/etc/rc.d/rc.$PRGNAM.new
install -D -m 0644 $CWD/$PRGNAM.conf $PKG/etc/$PRGNAM/$PRGNAM.conf.new
install -D -m 0644 $CWD/$PRGNAM.logrotate $PKG/etc/logrotate.d/lighttpd.new

chown -R $LIGHTTPD_USER:$LIGHTTPD_GROUP $PKG/var/log/lighttpd/
chown -R $LIGHTTPD_USER:$LIGHTTPD_GROUP $PKG/var/cache/lighttpd

find $PKG/usr/man -type f -exec gzip -9 {} \;

mkdir -p $PKG/usr/doc/$PRGNAM-$VERSION
cp -a AUTHORS COPYING INSTALL NEWS README doc/* \
  $PKG/usr/doc/$PRGNAM-$VERSION
rm -f $PKG/usr/doc/$PRGNAM-$VERSION/Makefile*
cat $CWD/README.SLACKWARE > $PKG/usr/doc/$PRGNAM-$VERSION/README.SLACKWARE
cat $CWD/$PRGNAM.SlackBuild > $PKG/usr/doc/$PRGNAM-$VERSION/$PRGNAM.SlackBuild

mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc
cat $CWD/doinst.sh > $PKG/install/doinst.sh

cd $PKG
echo "Finding dependencies..."
ADD="$ADDRB" EXCLUDE="$EXRB" requiredbuilder -v -y -s $OUTPUT $PKG
cat $CWD/slack-desc > $OUTPUT/slack-desc

/sbin/makepkg -l y -c n $OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.${PKGTYPE:-txz}
fi
