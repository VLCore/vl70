#!/bin/sh

#
# Script for launching Sigmira.  Suitable for placement in /usr/bin.
#
#

sigmira_root=/usr/share/sigmira

cd $sigmira_root/bin
./sigmira
