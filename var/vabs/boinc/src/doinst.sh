#!/bin/sh

# Place BOINC INIT script.
config() {
  NEW="$1"
  OLD="`dirname $NEW`/`basename $NEW .new`"
  # If there's no config file by that name, mv it over:
  if [ ! -r $OLD ]; then
    mv $NEW $OLD
  elif [ "`cat $OLD | md5sum`" = "`cat $NEW | md5sum`" ]; then # toss the redundant copy
    rm $NEW
  fi
  # Otherwise, we leave the .new copy for the admin to consider...
}


config etc/sysconfig/boinc-client.new

# create boinc user and group if not exist
if ! grep "^boinc:" etc/group >/dev/null 2>&1; then
	/usr/sbin/groupadd boinc 2>/dev/null || true
fi

if ! grep "^boinc:" etc/passwd >/dev/null 2>&1; then
	/usr/sbin/useradd -s /bin/bash -g boinc -d /var/lib/boinc boinc 2>/dev/null || true
fi

# create dirs if it not exist
if [ ! -d /var/lib/boinc ]; then
    install -d /var/lib/boinc
fi
chown -R boinc:boinc /var/lib/boinc


